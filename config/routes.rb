Rails.application.routes.draw do

  devise_for :admins
  use_doorkeeper

  authenticated :admin do
    root to: 'admin/users#index', as: :authenticated_root
  end
  root to: redirect('/admins/sign_in')

  resources :confirm_emails

  namespace :admin do
    resources :users
    resources :events
    resources :performers
    resources :groups
    resources :venues
  end

  namespace :api do
    namespace :v1 do
      resources :users
      resources :venues
      resources :performers
      resources :group_performers
      resources :groups
      resources :news
      resources :activities
      resources :tickets do
        member do
          post :accept
          post :reject
        end
      end
      resources :events do
        resources :event_participants
      end
    end
  end

  get "callbacks/gmail", to: "callbacks#gmail"
  get "pages/gmail", to: "pages#gmail"

end
