# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@klika.ba" }
    sequence(:nickname) { |n| "person#{n}" }
    password 'password!'
    password_confirmation 'password!'
    confirmed_at Time.now
    image 'image'
    image_small 'image'
    image_profile 'image'
    image_format 'jpg'
  end
end
