# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Venue, type: :request do

  describe 'create venue' do
    let!(:user) { User.create(first_name: 'fname', last_name: 'lname', email: 'person@mail.com', username: 'user123', password: 'password!', password_confirmation: 'password!',  confirmed_at: Time.now) }
    let!(:application) { Doorkeeper::Application.create(name: 'test', redirect_uri: 'https://localhost:3000') }
    let!(:token) { Doorkeeper::AccessToken.create(application: application, resource_owner_id: user.id) }
    let!(:venue) { Venue.create(name: 'name', user_id: user.id, description: 'desc') }

    let!(:payload) do
      {
        venue: {
          name: 'venue',
          description: 'desc of venue',
          address: 'Barcelona stadium',
          base64_photo: 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg=='
        }
      }
    end

    it 'returns venues' do
      get '/api/v1/venues',
          headers: { 'Authorization': "bearer #{token.token}" },
          as: :json
      expect(JSON.parse(response.body)["venues"].count).to equal 1
    end

    it 'creates venue' do
      post '/api/v1/venues',
          headers: { 'Authorization': "bearer #{token.token}" },
          params: payload,
          as: :json
      expect(response).to have_http_status(201)
    end


    it 'cannot create venue' do
      post '/api/v1/venues',
          headers: { 'Authorization': "bearer #{token.token}" },
          params: { venue: {name: ''}},
          as: :json
      expect(response).to have_http_status(422)
    end

    it 'unauthorized create venue' do
      post '/api/v1/venues',
          headers: { 'Authorization': "bearer #{token.token}+1" },
          params: { performer: {name: ''}},
          as: :json
      expect(response).to have_http_status(401)
    end
  end
end
