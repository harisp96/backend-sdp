# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :request do

  describe 'create user' do

    describe 'valid data' do
      let!(:payload) do
        {
          user: {
            first_name: 'user',
            last_name: 'test',
            username: 'username',
            email: 'user@klika.ba',
            password: 'password!',
            password_confirmation: 'password!'
          }
        }
      end

      it 'creates new user' do
        expect { post '/api/v1/users', params: payload, as: :json }
          .to change(User, :count).by(1)
      end

      it 'cannot create new user' do
         post '/api/v1/users', params: { user: { name: ''} }, as: :json
         expect(response).to have_http_status(422)
      end

      it 'returns status 200' do
        post '/api/v1/users', params: payload, as: :json
        expect(response).to have_http_status(200)
      end
    end
  end

  describe 'sign in' do
    let!(:user) { User.create(first_name: 'fname', last_name: 'lname', email: 'person@mail.com', username: 'user123', password: 'password!', password_confirmation: 'password!',  confirmed_at: Time.now) }
    let!(:application) { Doorkeeper::Application.create(name: 'test', redirect_uri: 'https://localhost:3000') }
    let!(:payload) do
      {
        username: user.email,
        password: 'password!',
        client_id: application.uid,
        client_secret: application.secret,
        grant_type: "password"
      }
    end

    it 'logges in and gets access token' do
      post '/oauth/token', params: payload, as: :json
      expect(response).to have_http_status(200)
    end

    it 'cannot log in' do
      post '/oauth/token', params: {}, as: :json
      expect(response).to have_http_status(401)
    end
  end
end
