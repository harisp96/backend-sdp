# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Performer, type: :request do

  describe 'create performer' do
    let!(:user) { User.create(first_name: 'fname', last_name: 'lname', email: 'person@mail.com', username: 'user123', password: 'password!', password_confirmation: 'password!',  confirmed_at: Time.now) }
    let!(:application) { Doorkeeper::Application.create(name: 'test', redirect_uri: 'https://localhost:3000') }
    let!(:token) { Doorkeeper::AccessToken.create(application: application, resource_owner_id: user.id) }
    let!(:payload) do
      {
        performer: {
          first_name: 'perf',
          last_name: 'performer',
          bio: 'bio of perf',
          base64_photo: 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg=='
        }
      }
    end

    it 'creates performer' do
      post '/api/v1/performers',
          headers: { 'Authorization': "bearer #{token.token}" },
          params: payload,
          as: :json
      expect(response).to have_http_status(201)
    end


    it 'cannot create performer' do
      post '/api/v1/performers',
          headers: { 'Authorization': "bearer #{token.token}" },
          params: { performer: {first_name: ''}},
          as: :json
      expect(response).to have_http_status(422)
    end

    it 'unauthorized create performer' do
      post '/api/v1/performers',
          headers: { 'Authorization': "bearer #{token.token}+1" },
          params: { performer: {first_name: ''}},
          as: :json
      expect(response).to have_http_status(401)
    end
  end
end
