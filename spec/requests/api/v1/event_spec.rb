# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Event, type: :request do

  describe 'create event' do
    let!(:user) { User.create(first_name: 'fname', last_name: 'lname', email: 'person@mail.com', username: 'user123', password: 'password!', password_confirmation: 'password!',  confirmed_at: Time.now) }
    let!(:application) { Doorkeeper::Application.create(name: 'test', redirect_uri: 'https://localhost:3000') }
    let!(:token) { Doorkeeper::AccessToken.create(application: application, resource_owner_id: user.id) }
    let!(:event) { Event.create(name: 'name', user_id: user.id, description: 'desc') }

    let!(:payload) do
      {
        event: {
          name: 'event',
          description: 'desc of event',
          base64_photo: 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8/5+hHgAHggJ/PchI7wAAAABJRU5ErkJggg=='
        }
      }
    end

    it 'returns events' do
      get '/api/v1/events',
          headers: { 'Authorization': "bearer #{token.token}" },
          as: :json
      expect(JSON.parse(response.body)["events"].count).to equal 1
    end

    it 'creates event' do
      post '/api/v1/events',
          headers: { 'Authorization': "bearer #{token.token}" },
          params: payload,
          as: :json
      expect(response).to have_http_status(201)
    end


    it 'cannot create event' do
      post '/api/v1/events',
          headers: { 'Authorization': "bearer #{token.token}" },
          params: { event: {name: ''}},
          as: :json
      expect(response).to have_http_status(422)
    end

    it 'unauthorized create event' do
      post '/api/v1/events',
          headers: { 'Authorization': "bearer #{token.token}+1" },
          params: { performer: {first_name: ''}},
          as: :json
      expect(response).to have_http_status(401)
    end
  end
end
