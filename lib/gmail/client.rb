
module Gmail
  class Client
    CC_REGEXP = /\Acc\z/i

    attr_accessor :client_id
    attr_accessor :client_secret
    attr_accessor :access_token
    attr_accessor :refresh_token
    attr_accessor :client
    attr_accessor :gmail
    attr_accessor :instance_url
    attr_accessor :token_type

    def initialize(options={})
      options.symbolize_keys!
      @client_id = options[:client_id] || ENV["GOOGLE_CLIENT_ID"]
      @client_secret = options[:client_secret] || ENV["GOOGLE_CLIENT_SECRET"]
      @redirect_uri = options[:redirect_uri] || ENV["GOOGLE_REDIRECT_URI"]

      if options.has_key?(:access_token)
        @access_token = options[:access_token]
        @refresh_token = options[:refresh_token]
      end
    end

    def get_logged_in_user
      uri = URI("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=%{access_token}" % {
        access_token: access_token
        })
      res = Net::HTTP.get_response(uri)
      result = JSON.parse(res.body).symbolize_keys

    end

    def client
      @client ||
      @client = Google::APIClient.new(
        application_name: ENV["APP_NAME"],
        application_version: ENV["APP_VERSION"]
      )
      @client.authorization.client_id = @client_id
      @client.authorization.client_secret = @client_secret
      @client.authorization.access_token = @access_token
      @client.authorization.grant_type = "refresh_token"
      @client.authorization.refresh_token = @refresh_token
      @client.authorization.fetch_access_token!
      @client
    end

    def authenticate(options={})
      request   = https_request("https://accounts.google.com")
      data      = {code: options[:code], client_id: @client_id, client_secret: @client_secret, redirect_uri: @redirect_uri, grant_type: "authorization_code"}
      response  = request.post("/o/oauth2/token", URI.encode_www_form(data), {"Content-Type" => "application/x-www-form-urlencoded"}).body
      result    = JSON.parse(response)

      self.access_token = result["access_token"]
      self.refresh_token = result["refresh_token"]
      self.instance_url = result["instance_url"]
      self.token_type = result["token_type"]
    end

    def api
      @api ||= client.discovered_api("gmail", "v1")
    end

    def plus_api
      @plus_api ||= client.discovered_api("plus", "v1")
    end

    # @return [Hash] or [NilClass]
    # Example:
    #   client.contact_profile_by_email('muamer.ribica@klika.ba', %w{emailAddresses})
    def contact_profile_by_email(email, fields=[])
      resource_name = resource_name_for_email(email)
      people_get(resource_name, fields) if resource_name
    end

    # TODO implement nextPageToken search
    # This will search for email among first 500 user contacts
    # To be more accurate, we need to iterate over all contacts.
    # Grab nextPageToken and query with people_list(%w{phoneNumbers}, {pageToken: nextPageToken})
    # @return [String] or [NilClass]
    def resource_name_for_email(email)
      return if email.blank?
      email = email.downcase
      people = Array(people_list(%w{email_addresses})["connections"])
      people.find(->{{}}) do |person|
        Array(person["emailAddresses"]).find(->{false}) do |a|
          a["value"].to_s.downcase == email
        end
      end["resourceName"]
    end

    def people_api
      @_people_api ||= client.discovered_api("people", "v1").tap do |a|
        a.method_base = "https://people.googleapis.com"
      end
    end

    # client.people_list
    # client.people_list(%w{addresses email_addresses phone_numbers})
    # https://developers.google.com/people/api/rest/v1/people#resource-person
    def people_list(fields=[], options={})
      options = {pageSize: 500}.merge(options.symbolize_keys)
      client.execute(
        # https://developers.google.com/people/api/rest/v1/people.connections/list#query-parameters
        api_method: people_api.people.connections.list,
        parameters: {
          resourceName: "people/me",
          pageToken: options[:pageToken],
          pageSize: options[:pageSize], # Valid values are between 1 and 500, inclusive. Defaults to 100.
          "requestMask.includeField": transform_people_fields(fields)
        }
      )
    end

    def people_get(resource_name, fields=[])
      client.execute(
        api_method: people_api.people.get,
        parameters: {
          resourceName: resource_name, # "people/me"
          "requestMask.includeField": transform_people_fields(fields)
        }
      )
    end

    def calendar_api
      @calendar_api ||= client.discovered_api("calendar","v3")
    end

    def https_request(url)
      Net::HTTP.new(URI.parse(url).host, 443).tap do |http|
        http.use_ssl = true
      end
    end

    def profile
      response = client.execute(plus_api.people.get, {'userId': 'me'})
      JSON.parse(response.body)
    end

    def messages(filter=nil)
      result = []

      request = ->(*args) do
        r = client.execute(messages_options(*args))
        Yajl::Parser.parse(r.body)
      end

      response = request.(filter)

      msgs = response["messages"] || []

      # https://developers.google.com/gmail/api/v1/reference/users/messages/list
      while (token = response["nextPageToken"]).present? do
        response = request.(filter, token)
        msgs.concat(Array(response["messages"]))
      end

      batch = Google::APIClient::BatchRequest.new do |r|
        parsed = Yajl::Parser.parse(r.body)
        message = GmailParser.new(parsed).()
        if (message[:email_type] != "draft") && message[:recipients] && message[:sender] && message[:sent_at]
          result << message
        end
      end

      # Note: You're limited to 1000 calls in a single batch request.
      # Many APIs enforce stricter quotas in addition to that limit.
      # If you need to make more calls than an API allows, use multiple batch requests.
      # https://developers.google.com/api-client-library/ruby/guide/batch
      msgs.each_slice(900) do |slice|
        slice.each do |message|
          batch.add(message_options_with_body(message['id']))
        end
        client.execute(batch)
        yield result if block_given?
        result = []
        batch.calls.clear
      end
    end

    def calendars
      response = client.execute(calendar_options)
      calendars = Yajl::Parser.parse(response.body)
      calendars['items']&.map do |c|
        { uid: c['id'], name: c['summary'] }
      end || []
    end

    def events(calendar_id)
      response = client.execute(event_options(calendar_id))
      return unless response.status == 200
      json = Yajl::Parser.parse(response.body)
      CalendarParser.new(json, calendar_id).call
    end

    def messages_options(filter, page_token=nil)
      {
        api_method: api.users.messages.list,
        parameters: {
          userId: "me",
          maxResults: 500,
          pageToken: page_token,
          q: filter
        }
      }
    end

    def message_options(message_id)
      {
        api_method: api.users.messages.get,
        parameters: {
          id: message_id,
          userId: "me",
          format: "metadata",
          metadataHeaders: %w{ Date Subject From To Cc },
        }
      }
    end

    def message_options_with_body(message_id)
      {
        api_method: api.users.messages.get,
        parameters: {
          id: message_id,
          userId: "me",
          format: "full",
        }
      }
    end

    def deliver(messages=[])
      batch = Google::APIClient::BatchRequest.new(options: { retries: 5 }) do |response|
        result = {
          payload: response.request.parameters[:payload],
          delivered: response.status == 200,
          body: response.body
        }
        puts "-------------------"
        puts result
        puts "-------------------"
        yield result if block_given?
      end

      messages.each_slice(5) do |slice|
        slice.each do |message|
          m = Mail.new
          m.from = message[:from]
          m.to = "#{message[:to][:name]} <#{message[:to][:address]}>"
          m.date = Time.now
          m.subject = message[:subject]

          html_part = Mail::Part.new do
            content_type 'text/html; charset=UTF-8'
            body message[:body]
          end

          m.html_part = html_part
          batch.add({
            api_method: api.users.messages.to_h['gmail.users.messages.send'],
            body_object: {
              raw: Base64.urlsafe_encode64(m.to_s)
            },
            parameters: {userId: "me", payload: message[:payload]}
          })
        end

        client.execute(batch)
        batch.calls.clear
        sleep(1)
      end
    end

    def send_message(options)
      msg = Mail.new
      msg.date = Time.now
      msg.subject = options[:subject]
      msg.body = options[:body]
      msg.to = options[:to]
      msg.header['X-Bcc'] = options[:bcc] if options[:bcc].present?

      client.execute(
        api_method: api.users.messages.to_h['gmail.users.messages.send'],
        body_object: {
          raw: Base64.urlsafe_encode64(msg.to_s.sub("X-Bcc", "Bcc"))
        },
        parameters: {userId: "me"}
      )
    end

    def attributes
      {
        uid: profile["id"],
        email: profile["emails"].first["value"],
        access_token: access_token,
        refresh_token: refresh_token,
        token_type: token_type
      }
    end

    def user_id
      profile["id"]
    end

    private

      def transform_people_fields(fields)
        Array(fields).reject(&:blank?).map do |field|
          "person.#{field}"
        end * ?,
      end

    def calendar_options
      {
        api_method: calendar_api.calendar_list.list,
        parameters: {
          minAccessRole: "owner",
          fields: "items"
        },
        headers: {'Content-Type' => 'application/json'}
      }
    end

    def event_options(calendar_id)
      {
        api_method: calendar_api.events.list,
        parameters:{
          calendarId: "#{calendar_id}",
          timeMax: (Date.today + 28).rfc3339,
          timeMin: Date.today.rfc3339
        },
        headers: {'Content-Type' => 'application/json'}
      }
    end
  end

  class GmailParser
    def initialize(response)
      @uid = response["id"]
      @payload = response["payload"]
      @type = response["labelIds"] && response["labelIds"].include?("SENT") ? "outbox" : "inbox"
      @type = "draft" if (response["labelIds"] && response["labelIds"].include?("DRAFT"))
    end

    def call
      from = header_for('From')
      sanitized_from = to_ascii(from)
      sender = Mail::Address.new(sanitized_from) rescue nil
      {
        uid: @uid,
        email_type: @type,
        subject: ( header_for('Subject') || "" ),
        sender: sender && sender.address && { name: sender.name, address: sender.address.downcase },
        recipients: recipients,
        sent_at: sent_at,
        body: body.to_s.encode('UTF-8', invalid: :replace, undef: :replace, replace: " ")
      }
    end

    def sent_at
      date_header = header_for('Date')
      date = date_header && Date.parse(date_header) rescue nil
      date_header if date
    end

    def recipients
      to = []
      to += Mail::AddressList.new(to_ascii header_for('To')).addresses
      to += Mail::AddressList.new(to_ascii header_for_cc).addresses if header_for_cc
      to.map! do |c|
        { name: c.display_name, address: c.address.try(:downcase) }
      end
      to
    rescue Mail::Field::ParseError
      Rollbar.warning("Gmail::Client#participants can't parse recipients", { to: header_for('To'), cc: header_for_cc})
      to
    end

    def header_for(key)
      item = @payload["headers"].find { |header| header.fetch("name".freeze, "".freeze) == key } if @payload
      return unless item
      item["value"]
    end

    # Research shows that name can be different
    # {"name"=>"Cc", "value"=>""}
    # {"name"=>"CC", "value"=>""}
    def header_for_cc
      cc = @payload && @payload["headers"].find(->{{}}) do |header|
        header["name"] =~ Gmail::Client::CC_REGEXP
      end["value"]
      return unless cc && cc['@']
      cc
    end

    private
      def body
        return unless @payload
        b = @payload.fetch("body".freeze, {})["data"]
        return decoded(b) if b
        parts = @payload["parts"].deep_dup
        return unless parts
        all_parts = []
        while parts.size > 0 do
          part = parts.shift
          parts.concat(part["parts"]) if part["parts"]
          all_parts << part
        end
        part = all_parts.find{ |part| part["mimeType"] == "text/plain".freeze }
        part ||= all_parts.find{ |part| part["mimeType"] == "text/html".freeze }
        return decoded(part["body"]["data"]) if part && part["body"]
      end

      def decoded(body)
        Base64.urlsafe_decode64(body) rescue nil
      end

      def to_ascii(string)
        return unless string
        string.encode('ASCII', invalid: :replace, undef: :replace, replace: " ")
      end
  end

  class CalendarParser
    def initialize(response, calendar_id)
      @calendar_id= calendar_id
      @calendar = {}
      @response = response
      @events = []
    end

    def call
      @calendar["calendarIdentifier"] = @calendar_id
      @calendar["events"] = parse_events
      @calendar["title"] = @response['summary']
      @calendar["type"] = 'spiro'
      @calendar["provider"] = "spiro"
      @calendar
    end

    def parse_events
      return if @response['error']
      @response['items'].each do |r|
        @events << {
          "eventIdentifier" => r['id'],
          "startDate"       => r['start'] && r['start']['dateTime'],
          "endDate"         => r['end'] && r['end']['dateTime'],
          "location"        => r['location'] || '',
          "title"           => r['summary'],
          "notes"           => r['description'],
          "participants"    => parse_participants(r['attendees'])
        }
      end
      @events
    end

    def parse_participants(participants)
      return if participants.nil?
      @participants = []
      participants.each do |p|
        @participants << {
          "email" => p['email'],
          "name" => p['displayName'],
          "participantRole" => "participant",
          "participantStatus" => p['responseStatus']
        }.compact
      end
      @participants
    end
  end
end
