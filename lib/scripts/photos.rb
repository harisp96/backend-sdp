module Scripts
  class Photos
    def call
      start = Time.now

      @client = DropboxApi::Client.new("gQNJFjjAjQAAAAAAAAAACvaI1D4vXAXkTLvYnH7yiX4TIXUs_lFc0InI2uXVvbiJ")

      Event.find_each do |e|
        file_url = @client.get_temporary_link(e.photo_name)
        e.update(photo_url: file_url.link)
      end

      Group.find_each do |g|
        file_url = @client.get_temporary_link(g.photo_name)
        g.update(photo_url: file_url.link)
      end

      Performer.find_each do |p|
        file_url = @client.get_temporary_link(p.photo_name)
        p.update(photo_url: file_url.link)
      end

      Venue.find_each do |v|
        file_url = @client.get_temporary_link(v.photo_name)
        v.update(photo_url: file_url.link)
      end

# code to time
finish = Time.now

diff = finish - start

    end
  end
end
