class AddPhotoNameAndUrlToVenues < ActiveRecord::Migration[5.1]
  def change
    add_column :venues, :photo_name, :string
    add_column :venues, :photo_url, :string
  end
end
