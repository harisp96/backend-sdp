class CreateActivities < ActiveRecord::Migration[5.1]
  def change
    create_table :activities do |t|
      t.integer :resource_id
      t.integer :resource_type
      t.timestamps
    end
  end
end
