class AddPhotoNameAndUrl < ActiveRecord::Migration[5.1]
  def change
    add_column :performers, :photo_name, :string
    add_column :performers, :photo_url, :string
  end
end
