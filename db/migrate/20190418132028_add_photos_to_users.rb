class AddPhotosToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :photo_name, :string
    add_column :users, :photo_url, :string
  end
end
