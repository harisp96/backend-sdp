class CreateGroupPerformers < ActiveRecord::Migration[5.1]
  def change
    create_table :group_performers do |t|
      t.references :group, index: true, foreign_key: true
      t.references :performer, index: true, foreign_key: true
      t.timestamps
    end
  end
end
