class CreateEventParticipants < ActiveRecord::Migration[5.1]
  def change
    create_table :event_participants do |t|
      t.integer :resource_id
      t.string :resource_type
      t.references :event, index: true, foreign_key: true
      t.timestamps
    end
  end
end
