class AddPhotoUrlAndNameToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :photo_name, :string
    add_column :events, :photo_url, :string
  end
end
