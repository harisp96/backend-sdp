class AddPhotoUrlAndPathToGroups < ActiveRecord::Migration[5.1]
  def change
    add_column :groups, :photo_name, :string
    add_column :groups, :photo_url, :string

  end
end
