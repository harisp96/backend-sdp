class AddTicketNumberToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :number_of_tickets, :integer, default: 0
  end
end
