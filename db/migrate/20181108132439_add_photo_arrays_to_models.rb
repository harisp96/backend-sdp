class AddPhotoArraysToModels < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :photos, :text, array: true, default: []
    add_column :venues, :photos, :text, array: true, default: []
    add_column :groups, :photos, :text, array: true, default: []
    add_column :performers, :photos, :text, array: true, default: []
  end
end
