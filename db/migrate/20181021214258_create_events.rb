class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.references :venue, index: true
      t.timestamps
    end
  end
end
