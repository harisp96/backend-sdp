class RemoveNullConstraintFromEvent < ActiveRecord::Migration[5.1]
  def change
    change_column_null :events, :venue_id, true
  end
end
