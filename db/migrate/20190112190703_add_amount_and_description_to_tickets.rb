class AddAmountAndDescriptionToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :amount, :integer
    add_column :tickets, :description, :string
  end
end
