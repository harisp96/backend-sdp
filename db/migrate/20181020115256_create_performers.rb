class CreatePerformers < ActiveRecord::Migration[5.1]
  def change
    create_table :performers do |t|
      t.string :first_name
      t.string :last_name
      t.string :type
      t.text :bio
      t.timestamps
    end
  end
end
