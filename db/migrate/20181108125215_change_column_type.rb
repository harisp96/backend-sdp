class ChangeColumnType < ActiveRecord::Migration[5.1]
  def change
    change_column :activities, :resource_type, :string
  end
end
