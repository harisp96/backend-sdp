class AddNumberOfReservedTicketsToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :number_of_reserved_tickets, :integer, default: 0
  end
end
