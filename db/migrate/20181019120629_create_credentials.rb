class CreateCredentials < ActiveRecord::Migration[5.1]
  def change
    create_table :credentials do |t|
      t.references :user, index: true, foreign_key: true
      t.string :access_token
      t.string :uid
      t.string :email
      t.timestamps
    end
  end
end
