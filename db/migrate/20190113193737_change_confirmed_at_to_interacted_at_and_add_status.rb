class ChangeConfirmedAtToInteractedAtAndAddStatus < ActiveRecord::Migration[5.1]
  def change
    rename_column :tickets, :confirmed_at, :interacted_at
    add_column :tickets, :status, :integer, default: 0
  end
end
