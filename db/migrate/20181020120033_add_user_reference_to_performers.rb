class AddUserReferenceToPerformers < ActiveRecord::Migration[5.1]
  def change
    add_reference :performers, :user, index: true
  end
end
