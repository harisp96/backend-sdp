class EventsSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :venue, :featured_photo, :date

  def venue
    {
      id: object&.venue&.id,
      name: object&.venue&.name
    }
  end

  def featured_photo
    object.photo_url
  end

  def description
    return object.description if object.description.length < 120 && object.description.present?
    return object.description[0..120].gsub(/\s\w+\s*$/,'...') unless !object.description.present?
    'Description missing'
  end
end
