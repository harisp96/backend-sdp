class GroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :bio, :featured_photo, :user_id

  def featured_photo
    object.photo_url.presence || "http://csb24.com/wp-content/uploads/2018/09/picture_not_available_400-300.png"
  end

end
