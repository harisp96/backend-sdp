class GroupsSerializer < ActiveModel::Serializer
  attributes :id, :name, :bio, :featured_photo

  def featured_photo
    object.photo_url || "http://csb24.com/wp-content/uploads/2018/09/picture_not_available_400-300.png"
  end

  def bio
    return object.bio if object.bio.length < 120 && object.bio.present?
    return object.bio[0..120].gsub(/\s\w+\s*$/,'...') unless !object.bio.present?
    'Bio missing'
  end

end
