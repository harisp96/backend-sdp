 class NewsSerializer < ActiveModel::Serializer
  attributes :object_id, :name, :description, :featured_photo, :user, :created_at, :ob_name, :link

  def object_id
    object.object.id
  end

  def featured_photo
    object.object.photo_url
  end

  def description
    object.object.try(:bio).presence || object.object.try(:description).presence
  end

  def name
    object.object&.name
  end

  def user
    {
      id: object.object.user.id,
      email: object.object.user.email,
      name: object.object.user.first_name,
      photo_url: object.object.user.photo_url.presence || "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"

      #object.object.user.profile_photo_url
    }
  end

  def ob_name
    object.object.class.name
  end

  def link
    "#{object.object.class.name.downcase}s/#{object.object.id}"
  end

end
