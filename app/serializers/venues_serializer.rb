class VenuesSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :address, :latitude, :longitude, :featured_photo, :capacity

  def featured_photo
    object.photo_url
  end

  def description
    return object.description if object.description.length < 120 && object.description.present?
    return object.description[0..120].gsub(/\s\w+\s*$/,'...') unless !object.description.present?
    'Description missing'
  end
end
