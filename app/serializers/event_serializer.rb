include ApplicationHelper

class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :venue, :featured_photo, :single_participants, :group_participants, :photos, :number_of_tickets, :tickets_remaining, :user_id, :date, :venue

  def tickets_remaining
    object.number_of_tickets - object.number_of_reserved_tickets
  end

  def venue
    {
      id: object&.venue&.id,
      name: object&.venue&.name
    }
  end

  def featured_photo
      object.photo_url
  end

  def venue
    {
      id: object&.venue_id,
      name: object&.venue&.name,
      description: object&.venue&.description,
      featured_photo: object&.venue&.photo_url
    }
  end

  def group_participants
    resp = []
    object.group_participants.each do |group|
      resp << {
        id: group&.id,
        name: group&.name,
        photo_url: group.photo_url,
        bio: shortener(group.bio)
      }
    end
    resp
  end

  def single_participants
    resp = []
    object.single_participants.each do |participant|
      resp << {
        id: participant&.id,
        full_name: participant&.full_name,
        photo_url: participant.photo_url,
        bio: shortener(participant.bio)
      }
    end
    resp
  end

  def shortener(text)
    return text if text.length < 180
    return text[0..180] + '...'
  end
end
