class TicketsSerializer < ActiveModel::Serializer
  attributes :id, :event_id, :featured_photo, :subject, :description, :status

  def featured_photo
    object.user.photo_url.presence || "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"
  end

  def subject
    "#{User.find(object.user_id).first_name} wants to reserve #{object.amount} tickets for #{object.event.name}"
  end
end
