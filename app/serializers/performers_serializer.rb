include ApplicationHelper

class PerformersSerializer < ActiveModel::Serializer
  attributes :id, :featured_photo, :bio, :first_name, :full_name

  def featured_photo
    object.photo_url.presence || "http://csb24.com/wp-content/uploads/2018/09/picture_not_available_400-300.png"
  end

  def bio
    bio_resolver(object.bio)
  end

  def full_name
    object.first_name + ' ' + object.last_name
  end

end
