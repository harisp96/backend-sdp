class PerformerSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :bio, :user, :featured_photo, :full_name

  def user
    {
      name: object.user.full_name,
      id: object.user.id
    }
  end

  def full_name
    object.full_name
  end


  def featured_photo
    object.photo_url
  end
end
