class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :created_at, :featured_photo, :venues, :groups, :performers, :events

  def featured_photo
    object.photo_url.presence || "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/1024px-No_image_available.svg.png"
  end

  def venues
    object.venues.count
  end

  def groups
    object.groups.count
  end

  def performers
    object.performers.count
  end

  def events
    object.events.count
  end
end
