class ActivitySerializer < ActiveModel::Serializer
  attributes :user, :resource


  def user
    usr = object.resource_type.constantize.find(object.resource_id).user
    {
      full_name: usr.first_name,
      id: usr.id
    }
  end

  def resource
    object.resource_type.constantize.find(object.resource_id)
  end
end
