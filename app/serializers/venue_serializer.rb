class VenueSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :address, :latitude, :longitude, :featured_photo

  def featured_photo
    object.photo_url.presence ||  "http://csb24.com/wp-content/uploads/2018/09/picture_not_available_400-300.png"
  end

end
