class Event::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    event = user.events.create! do |e|
      e.name        = params[:name]
      e.description = params[:description]
      e.venue_id    = params[:venue_id]
      e.photo_name  = params[:photo_name]
      e.photo_url   = params[:photo_url]
      e.date        = params[:date]
    end
    event
  end
end
