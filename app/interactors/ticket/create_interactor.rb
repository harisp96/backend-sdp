class Ticket::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    ticket = Ticket.create do |t|
      t.user_id     = user.id
      t.description = params[:description]
      t.amount      = params[:amount]
      t.event_id    = params[:event_id]
    end
    ticket
  end
end
