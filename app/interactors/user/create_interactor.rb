class User::CreateInteractor
  attr_accessor :model

  def initialize
    @model = User.new
  end

  def call(params)
    @model = User.create! do |u|
      u.first_name        = params[:first_name]
      u.last_name         = params[:last_name]
      u.email             = params[:email]
      u.profile_photo_url = params[:profile_photo_url]
      u.password          = SecureRandom.hex(32)
    end

    @model
  end
end
