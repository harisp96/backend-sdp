class Token::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call
    access_token = Doorkeeper::AccessToken.create!(
      application_id: Doorkeeper::Application.find_by(name: 'Events Social Media')&.id,
      resource_owner_id: user.id,
      refresh_token: generate_refresh_token,
      expires_in: Doorkeeper.configuration.access_token_expires_in.to_i,
    )
  end

  private

  def generate_refresh_token
    loop do
      token = SecureRandom.hex(32)
      break token unless Doorkeeper::AccessToken.exists?(refresh_token: token)
    end
  end

end
