class EventParticipant::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    event_participant = EventParticipant.create! do |ep|
      ep.event_id         = params[:event_id]
      ep.resource_id      = params[:resource_id]
      ep.resource_type    = params[:resource_type]
    end
    event_participant
  end
end
