class Performer::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    performer = user.performers.create! do |pr|
      pr.first_name   = params[:first_name]
      pr.last_name    = params[:last_name]
      pr.bio          = params[:bio]
      pr.photo_name   = params[:photo_name]
      pr.photo_url    = params[:photo_url]
    end
    performer
  end
end
