class Group::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    group = user.groups.create! do |g|
      g.name         = params[:name]
      g.bio          = params[:bio]
      g.photo_name   = params[:photo_name]
      g.photo_url    = params[:photo_url]
    end
    group
  end
end
