class GroupPerformer::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    group_performer = GroupPerformer.create! do |gp|
      gp.group_id = params[:group_id]
      gp.performer_id = params[:performer_id]
    end
    group_performer
  end
end
