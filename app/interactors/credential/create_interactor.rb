class Credential::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
    @model = Credential.new
  end

  def call(params)
    @model = Credential.create! do |c|
      c.user_id      = user.id
      c.access_token = params[:access_token]
      c.uid          = params[:uid]
      c.email        = params[:email]
    end
  end
end
