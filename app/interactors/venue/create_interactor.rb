class Venue::CreateInteractor
  attr_accessor :user

  def initialize(user)
    @user = user
  end

  def call(params)
    venue = user.venues.create! do |v|
      v.name         = params[:name]
      v.description  = params[:description]
      v.latitude     = params[:latitude]
      v.longitude    = params[:longitude]
      v.capacity     = params[:capacity]
      v.photo_name   = params[:photo_name]
      v.photo_url    = params[:photo_url]
    end
    venue
  end
end
