class EventParticipantCreateForm < Reform::Form
  property :resource_id
  property :resource_type
  property :event_id

  property :event_id
  validates :resource_type, presence: true

  validate :event_existance
  validate :resource_existance

  def event_existance
    errors.add(:event_id, :invalid, { value: event_id }) unless Event.find_by(id: event_id).present?
  end

  def resource_existance
    case resource_type
    when 'Performer'
      errors.add(:resource_id, :invalid, { value: resource_id }) unless Performer.find_by(id: resource_id).present?
    when 'Group'
      errors.add(:resource_id, :invalid, { value: resource_id }) unless ::Group.find_by(id: resource_id).present?
    else
      errors.add(:resource_type, :invalid, { value: resource_type })
    end
  end
end
