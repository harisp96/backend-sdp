class GroupPerformerCreateForm < Reform::Form
  property :group_id
  property :performer_id

  # TODO: create validation for existance

  validates :performer_id, presence: true
  validates :group_id, presence: true

end
