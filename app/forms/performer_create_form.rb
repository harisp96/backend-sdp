class PerformerCreateForm < Reform::Form
  property :first_name
  property :last_name
  property :bio


  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :bio, presence: true

end
