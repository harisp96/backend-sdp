class UserCreateForm < Reform::Form
  property :first_name
  property :last_name
  property :email
  property :username

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true
  validates :username, presence: true
  
  validate :username_uniqueness
  validate :email_uniqueness

  def username_uniqueness
    if username && User.where("LOWER(username) = ?", username.downcase).exists?
      errors.add(:username, :not_unique, { value: username })
    end
  end

  def email_uniqueness
    if email && User.where("LOWER(email) = ?", email.downcase).exists?
      errors.add(:email, :not_unique, { value: email })
    end
  end
end
