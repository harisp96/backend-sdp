class VenueCreateForm < Reform::Form
  property :name
  property :description
  property :latitude
  property :longitude

  validates :name, presence: true
  validates :description, presence: true

end
