class GroupCreateForm < Reform::Form
  property :name
  property :bio

  validates :name, presence: true
end
