class TicketCreateForm < Reform::Form
  property :event_id
  property :description
  property :amount

  validate :event_presence
  validate :amount_validity

  def event_presence
    errors.add(:event_id, :not_found, { value: event_id }) unless Event.find_by(id: event_id).present?
  end

  def amount_validity
    valid_am = Event.find(event_id).number_of_tickets
    errors.add(:amount, :out_of_range, { value: amount }) if amount > valid_am
  end
end
