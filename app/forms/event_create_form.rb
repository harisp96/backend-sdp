class EventCreateForm < Reform::Form
  property :name
  property :description
  property :venue_id

  validates :name, presence: true
  validates :description, presence: true

  validate :venue_existance

  def venue_existance
    return unless venue_id.present?
    errors.add(:venue_id, :invalid, { value: venue_id }) unless Venue.find_by(id: venue_id).present?
  end
end
