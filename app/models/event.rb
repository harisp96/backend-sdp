class Event < ApplicationRecord
  has_many :event_participants, dependent: :destroy
  has_many :activities, as: :object

  
  has_many :tickets
  belongs_to :user
  belongs_to :venue, optional: true

  def group_participants
    Group.where(id: self.event_participants.participant_groups.pluck(:resource_id))
  end

  def single_participants
    Performer.where(id: self.event_participants.single_participants.pluck(:resource_id))
  end

  def bio
    description
  end
end
