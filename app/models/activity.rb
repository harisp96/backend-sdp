class Activity < ApplicationRecord
  belongs_to :object, polymorphic: true, foreign_type: 'resource_type', foreign_key: 'resource_id'
end
