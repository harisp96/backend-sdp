class Group < ApplicationRecord
  belongs_to :user
  has_many :activities, as: :object
  has_many :event_participants, as: :model
  has_many :group_performers
  has_many :performers, through: :group_performers, dependent: :destroy
  has_many :events, through: :event_participants

end
