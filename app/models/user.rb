class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_one :credential, dependent: :destroy
  has_secure_password
  has_many :venues, dependent: :destroy
  has_many :performers, dependent: :destroy
  has_many :groups, dependent: :destroy
  has_many :events, dependent: :destroy
  has_many :tickets, dependent: :destroy

  has_many :access_tokens, class_name: 'Doorkeeper::AccessToken',
                             foreign_key: :resource_owner_id,
                             dependent: :delete_all

  def get_valid_token
    Doorkeeper::AccessToken.where(resource_owner_id: id).each do |token|
      return token unless token.expired?
    end
    return nil
  end

  def full_name
    return first_name + ' ' + last_name
  end
end
