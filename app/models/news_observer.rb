class NewsObserver < ActiveRecord::Observer
  observe :group, :venue, :event, :performer

  def after_create(record)
    Activity.create(resource_id: record.id, resource_type: record.class.name, user_id: record.user.id)
  end

end
