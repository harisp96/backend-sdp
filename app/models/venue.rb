class Venue < ApplicationRecord
  belongs_to :user
  has_many :events
  has_many :activities, as: :object
  reverse_geocoded_by :latitude, :longitude
  after_validation :reverse_geocode
end
