class GroupPerformer < ApplicationRecord
  belongs_to :group
  belongs_to :performer

  has_many :event_participants, -> { where(resource_type: "Group") }, class_name: "EventParticipant", foreign_key: 'resource_id'
  has_many :events, through: :event_participants
end
