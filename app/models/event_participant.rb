class EventParticipant < ApplicationRecord
  scope :participant_groups, -> { where(resource_type: 'Group') }
  scope :single_participants, -> { where(resource_type: 'Performer') }


  belongs_to :resource, polymorphic: true
  belongs_to :event
end
