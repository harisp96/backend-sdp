class Performer < ApplicationRecord
  belongs_to :user
  has_many :activities, as: :object

  has_many :group_performers, dependent: :destroy
  has_many :groups, through: :group_performers

  has_many :event_participants, as: :model

  has_many :event_participants, -> { where(resource_type: "Performer") }, class_name: "EventParticipant", foreign_key: 'resource_id'
  has_many :events, through: :event_participants

  def full_name
    first_name + ' ' + last_name
  end

  def name
    full_name
  end
end
