class GroupQuery
  def initialize(relation = Group.all)
    @relation = relation.extending(Scopes)
  end

  def search
    @relation
  end

  module Scopes

    def filter_by(params)
      groups = self
      groups = groups.from_performer(params[:performer_id]) if params[:performer_id]
      groups = groups.without_event(params[:without_event]) if params[:without_event]
      groups
    end

    def from_performer(performer_id)
      joins(:group_performers)
        .where('group_performers.performer_id = ?', performer_id)
    end

    def without_event(event_id)
      event_participants = Event.find_by(id: event_id).event_participants
      group_ids_to_exclude = event_participants.where(resource_type: 'Group').pluck(:resource_id)
      event_participants.where(resource_type: 'Performer').each do |single|
        single.resource.groups.pluck(:id).each do |id|
          group_ids_to_exclude << id
        end
      end

      group_ids_to_exclude = [0] unless group_ids_to_exclude.present?
      where('groups.id not in (?)', group_ids_to_exclude.uniq)
    end

  end
end
