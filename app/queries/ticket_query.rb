class TicketQuery
  def initialize(relation = Ticket.all)
    @relation = relation.extending(Scopes)
  end

  def search
    @relation
  end

  module Scopes
    def filter_by(params)
      tickets = self
      tickets = tickets.by_user(params[:user_id]) if params[:user_id]
      tickets = tickets.for_user(params[:for_user_id]) if params[:for_user_id]
      tickets
    end

    def by_user(user_id)
      where(user_id: user_id)
    end

    def for_user(user_id)
      users_events_ids = Event.where(user_id: user_id).ids
      if users_events_ids.count == 0
        users_events_ids << 0
      end
      where(status: 0).where('event_id in (?)', users_events_ids)
    end
  end
end
