class PerformerQuery
  def initialize(relation = Performer.all)
    @relation = relation.extending(Scopes)
  end

  def search
    @relation
  end

  module Scopes

    def filter_by(params)
      performers = self
      performers = performers.from_group(params[:group_id])                 if params[:group_id]
      performers = performers.not_from_group(params[:not_group_id])         if params[:not_group_id]
      performers = performers.without_event(params[:without_event])         if params[:without_event]

      performers
    end

    def from_group(group_id)
      joins(:group_performers)
      .where('group_performers.group_id = ?', group_id)
    end

    def not_from_group(group_id)
      ids = Performer.joins(:group_performers)
      .where('group_performers.group_id = ?', group_id).ids
      if ids.count == 0
        ids << 0
      end
      where('id NOT IN (?)', ids)
    end


    def without_event(event_id)

      event_participants = Event.find_by(id: event_id).event_participants
      performer_ids_to_exlude = event_participants.where(resource_type: 'Performer').pluck(:resource_id)
      event_participants.where(resource_type: 'Group').each do |group|
        group.resource.performers.pluck(:id).each do |id|
          performer_ids_to_exlude << id
        end
      end
      performer_ids_to_exlude = [0] unless performer_ids_to_exlude.present?

      where('performers.id not in (?)', performer_ids_to_exlude.uniq)
    end


  end
end
