class ActivityQuery
  def initialize(relation = Activity.all)
    @relation = relation.extending(Scopes)
  end

  def search
    @relation
  end

  module Scopes
    def filter_by(params)
      activities = self
      activities = activities.from_user(params[:user_id]) if params[:user_id]
      activities
    end

    def from_user(id)
      where(user_id: id)
    end
  end
end
