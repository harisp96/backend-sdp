class VenueQuery
  def initialize(relation = Venue.all)
    @relation = relation.extending(Scopes)
  end

  def search
    @relation
  end

  module Scopes

    def filter_by(params)
      venues = self
      venues = venues.from_event(params[:event_id]) if params[:event_id]
      venues
    end

    def from_event(event_id)
      joins(:events).where('events.id = ?', event_id )
    end

  end
end
