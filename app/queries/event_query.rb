class EventQuery
  def initialize(relation = Event.all)
    @relation = relation.extending(Scopes)
  end

  def search
    @relation
  end

  module Scopes

    def filter_by(params)
      events = self
      events = events.from_performer(params[:performer_id]) if params[:performer_id]
      events = events.from_group(params[:group_id])         if params[:group_id]
      events = events.from_venue(params[:venue_id])         if params[:venue_id]
      events
    end

    def from_performer(performer_id)
      joins(:event_participants)
        .where('event_participants.resource_type = ?', 'Performer')
        .where('event_participants.resource_id = ?', performer_id)
    end

    def from_group(group_id)
      joins(:event_participants)
        .where('event_participants.resource_type = ?', 'Group')
        .where('event_participants.resource_id = ?', group_id)
    end

    def from_venue(venue_id)
      where(venue_id: venue_id)
    end


  end
end
