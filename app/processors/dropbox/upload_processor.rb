class Dropbox::UploadProcessor
  attr_accessor :client, :file_name
  def initialize
    # TODO: Move to env variable
    @client = DropboxApi::Client.new("gQNJFjjAjQAAAAAAAAAACvaI1D4vXAXkTLvYnH7yiX4TIXUs_lFc0InI2uXVvbiJ")
    @file_name = "/photo_#{DateTime.now.to_i}.png"
  end

  def call(base64_photo)
    File.open('local_image.png', 'wb') do |f|
      f.write(Base64.decode64(base64_photo))
    end

    file = IO.read("local_image.png")

    image = client.upload(file_name, file)
    file_url = client.get_temporary_link(file_name)

    {
      photo_name: file_name,
      photo_url: file_url.link
    }
  end
end
