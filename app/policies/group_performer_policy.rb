class GroupPerformerPolicy < ApplicationPolicy

  def destroy?
    record.group.user_id == user.id && record.performer.user_id == user.id
  end

  def create?
    record.user_id == user.id
  end
end
