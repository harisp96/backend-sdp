class GroupPolicy < ApplicationPolicy

  def destroy?
    record.user_id == user.id
  end

end
