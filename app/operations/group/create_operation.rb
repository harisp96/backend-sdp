class Group::CreateOperation
  attr_accessor :user, :model, :contract, :errors

  def initialize(user)
    @user = user
    @model = Group.new
    @contract = GroupCreateForm.new(@model)
  end

  def call(params)
    if contract.validate(params)
      photo_params = Dropbox::UploadProcessor.new().(params[:base64_photo])
      model = Group::CreateInteractor.new(user).(params.except(:base64_photo).merge(photo_params))
      yield model
    else
      @errors = contract.errors
    end
    self
  end
end
