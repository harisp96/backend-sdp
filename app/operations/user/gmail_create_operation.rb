class User::GmailCreateOperation
  attr_accessor :gmail_api_response

  def initialize(gmail_api_response)
    @gmail_api_response = gmail_api_response
  end

  def call
    @user = User::CreateInteractor.new.(gmail_create_user_params)
    Credential::CreateInteractor.new(@user).(gmail_create_credential_params)
    return @user
  end

  private

  def gmail_create_user_params
    {
      first_name: gmail_api_response[:given_name],
      last_name: gmail_api_response[:family_name],
      email: gmail_api_response[:email],
      profile_photo_url: gmail_api_response[:picture]
    }
  end

  def gmail_create_credential_params
    {
      uid: gmail_api_response[:id],
      access_token: gmail_api_response[:access_token],
      email: gmail_api_response[:email],
    }
  end
end
