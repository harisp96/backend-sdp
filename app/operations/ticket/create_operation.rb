class Ticket::CreateOperation
  attr_accessor :user, :model, :contract, :errors

  def initialize(user)
    @user = user
    @model = Ticket.new
    @contract = TicketCreateForm.new(@model)
  end

  def call(params)
    if contract.validate(params)
      model = Ticket::CreateInteractor.new(user).(params)
      yield model
    else
      @errors = contract.errors
    end
    self
  end
end
