class EventParticipant::CreateOperation
  attr_accessor :user, :model, :contract, :errors

  def initialize(user)
    @user = user
    @model = EventParticipant.new
    @contract = EventParticipantCreateForm.new(@model)
  end

  def call(params)
    if contract.validate(params)
      model = EventParticipant::CreateInteractor.new(user).(params)
      yield model
    else
      @errors = contract.errors
    end
    self
  end
end
