class GroupPerformer::CreateOperation
  attr_accessor :user, :model, :contract, :errors

  def initialize(user)
    @user = user
    @model = GroupPerformer.new
    @contract = GroupPerformerCreateForm.new(@model)
  end

  def call(params)
    if contract.validate(params)
      model = GroupPerformer::CreateInteractor.new(user).(params)
      yield model
    else
      @errors = contract.errors
    end
    self
  end
end
