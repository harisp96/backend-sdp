class Api::V1::TicketsController < Api::V1::BaseController
  def index
    tickets = TicketQuery.new.search.filter_by(params).order(created_at: :desc)

    respond_with tickets, each_serializer: TicketsSerializer
  end

  def create
    operation = Ticket::CreateOperation.new(current_user).(create_ticket_params) do |model|
      UserMailer.with(request: model).reservation_request.deliver_now
      respond_with model, location: '/', status: 200 and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def accept
    ticket_reservation = Ticket.find(params[:id])
    event = ticket_reservation.event

    if event.number_of_tickets - event.number_of_reserved_tickets < ticket_reservation.amount
      render json: { message: "Your event does not have enough available seats to confirm this reservation" }, status: 422
    else
      ticket_reservation.update(status: 1)
      ticket_reservation.touch(:interacted_at)
      event.update(number_of_reserved_tickets: event.number_of_reserved_tickets + ticket_reservation.amount)
      UserMailer.with(request: ticket_reservation).confirm_reservation_request.deliver_now

    end

  end

  def reject
    ticket_reservation = Ticket.find(params[:id])
    ticket_reservation.update(status: 2)
    ticket_reservation.touch(:interacted_at)
    UserMailer.with(request: ticket_reservation).decline_reservation_request.deliver_now
  end


  private

  def create_ticket_params
    params.require(:ticket).permit(:event_id, :amount, :description)
  end
end
