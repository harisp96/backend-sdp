class Api::V1::VenuesController < Api::V1::BaseController
  def index
    venues = VenueQuery.new.search.filter_by(params)
    respond_with venues, each_serializer: VenuesSerializer
  end

  def show
    respond_with Venue.find(params[:id])
  end

  def create
    operation = Venue::CreateOperation.new(current_user).(create_venue_params) do |venue|
      respond_with venue, location: '/' and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def destroy
    venue = Venue.find(params[:id])
    authorize venue

    venue.destroy

    render nothing: true, status: :ok
  end

  private

  def create_venue_params
    params.require(:venue).permit(:name, :description, :capacity, :latitude, :longitude, :base64_photo)
  end
end
