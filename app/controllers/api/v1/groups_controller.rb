class Api::V1::GroupsController < Api::V1::BaseController
  def index
    groups = GroupQuery.new.search.filter_by(params)
    respond_with groups, each_serializer: GroupsSerializer
  end

  def show
    respond_with Group.find(params[:id])
  end

  def create
    operation = Group::CreateOperation.new(current_user).(create_group_params) do |group|
      respond_with group, location: '/' and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def destroy
    group = Group.find(params[:id])
    authorize group

    group.destroy

    render nothing: true, status: :ok
  end

  private

  def create_group_params
    params.require(:group).permit(:name, :bio, :base64_photo)
  end
end
