class Api::V1::EventParticipantsController < Api::V1::BaseController
  def index
    event = Event.find(params[:event_id])
    render json: {
        event_participants:
          {
            groups: event.group_participants,
            single_participants: event.single_participants
          }
        }

  end

  def show
  end

  def create
    operation = EventParticipant::CreateOperation.new(current_user).(create_event_participant_params.merge(event_id: params[:event_id])) do |event_participant|
      respond_with event_participant, location: '/' and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def destroy
  end

  private

  def create_event_participant_params
    params.require(:participant).permit(:resource_id, :resource_type)
  end
end
