class Api::V1::EventsController < Api::V1::BaseController
  def index
    events = EventQuery.new.search.filter_by(params).order(created_at: :desc)
    respond_with events, each_serializer: EventsSerializer
  end

  def show
    event = Event.find(params[:id])
    respond_with event, serializer: EventSerializer
  end

  def create
    operation = Event::CreateOperation.new(current_user).(create_event_params) do |event|
      respond_with event, location: '/' and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def destroy
    event = Event.find(params[:id])
    authorize event

    event.destroy

  end

  def update
    event = Event.find(params[:id])
    event.update(update_event_params)
    respond_with event
  end

  private

  def create_event_params
    params.require(:event).permit(:name, :description, :venue_id, :base64_photo, :date)
  end

  def update_event_params
    params.require(:event).permit(:venue_id, :number_of_tickets)
  end
end
