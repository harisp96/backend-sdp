class Api::V1::PerformersController < Api::V1::BaseController
  def index
    performers = PerformerQuery.new.search.filter_by(params)
    respond_with performers, each_serializer: PerformersSerializer
  end

  def show
    respond_with Performer.find(params[:id])
  end

  def create
    operation = Performer::CreateOperation.new(current_user).(create_performer_params) do |performer|
      respond_with performer, location: '/' and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def destroy
    performer = Performer.find(params[:id])
    authorize performer

    Performer.destroy

    render nothing: true, status: :ok
  end

  private

  def create_performer_params
    params.require(:performer).permit(:first_name, :last_name, :bio, :base64_photo)
  end
end
