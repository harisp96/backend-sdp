class Api::V1::NewsController < Api::V1::BaseController
  def index
    news = ActivityQuery.new.search.filter_by(params).order(created_at: :desc)
    respond_with news, each_serializer: NewsSerializer
  end

  def show
  end

end
