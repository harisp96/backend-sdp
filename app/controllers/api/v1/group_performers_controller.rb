class Api::V1::GroupPerformersController < Api::V1::BaseController
  def index
    group_performers = GroupPerformer.all
    respond_with group_performers
  end

  def show
    respond_with GroupPerformer.find(params[:id])
  end

  def create
    group = Group.find_by(id: create_group_performer_params[:group_id])
    authorize(group, policy_class: GroupPerformerPolicy)

    operation = GroupPerformer::CreateOperation.new(current_user).(create_group_performer_params) do |group_performer|
      respond_with group_performer, location: '/' and return
    end

    respond_with operation.errors, location: '/', status: 422
  end

  def destroy
    group_performer = GroupPerformer.find(params[:id])
    authorize group_performer

    group_performer.destroy

    render nothing: true, status: :ok
  end

  private

  def create_group_performer_params
    params.require(:group_performer).permit(:group_id, :performer_id)
  end
end
