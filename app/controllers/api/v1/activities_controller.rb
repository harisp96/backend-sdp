class Api::V1::ActivitiesController < Api::V1::BaseController
  def index
    respond_with Activity.all
  end
end
