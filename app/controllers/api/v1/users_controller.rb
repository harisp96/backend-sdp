
class Api::V1::UsersController < Api::V1::BaseController
  skip_before_action :doorkeeper_authorize!, only: [:create]

  def index
    users = User.all
    respond_with users
  end

  def show
    user = User.find(params[:id])
    respond_with user, serializer: UserSerializer
  end

  def create
    @user = User.new(user_params)
    form = UserCreateForm.new(@user)

    if form.validate(user_params)
      user = User.create(user_params)
      UserMailer.with(user: user).welcome_email.deliver_now
      render json: user
    else
      render json: form.errors, status: 422
    end
  end

  def update
    @user = User.find(params[:id])
    form = UserUpdateForm.new(@user)

    photo_params = Dropbox::UploadProcessor.new().(user_params[:base64_photo])

    @user.update(user_params.except(:base64_photo).merge(photo_params))
    render json: @user
  end

  def destroy
    User.destroy(params[:id])
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :username, :email, :password, :password_confirmation, :base64_photo)
  end

end
