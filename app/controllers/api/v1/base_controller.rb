
class Api::V1::BaseController < ActionController::API
  include Pundit
  respond_to :json
  before_action :doorkeeper_authorize!
  helper_method :current_user

  def current_user
    User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
  end

  rescue_from StandardError do |error|
    if error.is_a?(ActiveRecord::RecordNotFound)
      render json: { message: "Can't find this resource" }, status: 404
    elsif error.is_a?(Pundit::NotAuthorizedError)
      render json: { message: "You don't have privileges to access this resource" }, status: 403
    else
      report_rollbar(error)
      raise error
      render json: { message: error.message }, status: 500
    end
  end

  def report_rollbar(error)
    Rollbar.error(error, error.message) unless Rails.env.development?
  end

end
