class CallbacksController < ActionController::API

  def gmail
    puts 'in the method'
    client = Gmail::Client.new({client_id: ENV['GOOGLE_CLIENT_ID'] , client_secret: ENV['GOOGLE_CLIENT_SECRET'], redirect_uri: ENV['GOOGLE_REDIRECT_URI']})
    client.authenticate({code: params[:code]})
    user_attributes = client.get_logged_in_user

    user = User.find_by(email: user_attributes[:email]) || User::GmailCreateOperation.new(user_attributes.merge({ access_token: client.access_token })).()

    token = user.get_valid_token || Token::CreateInteractor.new(user).()

    redirect_to "https://www.9gag.com"

  end
end
