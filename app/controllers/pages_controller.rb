class PagesController < ActionController::API
  def gmail
    permissions = %w{
    https://www.googleapis.com/auth/userinfo.email
    https://www.googleapis.com/auth/userinfo.profile
    https://www.googleapis.com/auth/plus.login
  }

  # access token is users access token se we can identify to whom we should assign

  redirect_to "https://accounts.google.com/o/oauth2/auth?response_type=code&access_type=offline&client_id=%{client_id}&redirect_uri=%{redirect_uri}&scope=%{scope}&approval_prompt=force" % {
    client_id: ENV["GOOGLE_CLIENT_ID"],
    redirect_uri: ENV["GOOGLE_REDIRECT_URI"],
    scope: permissions.join('+'),
  }

  end
end
