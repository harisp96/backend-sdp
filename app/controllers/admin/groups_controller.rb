class Admin::GroupsController < Admin::BaseController
  def index
    @groups = Group.all
  end

  def show
    @group = Group.find(params[:id])
  end

  def destroy
    Group.find(params[:id]).destroy

    redirect_to :action => "index"  
  end
end
