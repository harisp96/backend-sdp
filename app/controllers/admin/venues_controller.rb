class Admin::VenuesController < Admin::BaseController
  def index
    @venues = Venue.all
  end

  def show
    @venue = Venue.find(params[:id])
  end

  def destroy
    Venue.find(params[:id]).destroy

      redirect_to :action => "index"  
  end
end
