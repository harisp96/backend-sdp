class Admin::UsersController < Admin::BaseController
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    @groups = Group.where(user_id: @user.id)
    @events = Event.where(user_id: @user.id)
    @performers = Performer.where(user_id: @user.id)
    @venues = Venue.where(user_id: @user.id)
  end
end
