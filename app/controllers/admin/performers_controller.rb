class Admin::PerformersController < Admin::BaseController
  def index
    @performers = Performer.all
  end

  def show
    @performer = Performer.find(params[:id])
  end

  def destroy
    Performer.find(params[:id]).destroy

      redirect_to :action => "index"  
  end
end
