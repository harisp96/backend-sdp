class ConfirmEmailsController < ApplicationController
  def index
    @user = User.find_by(confirmation_token: params[:token])
    @user.update(confirmation_token: nil, confirmation_sent_at: nil, confirmed_at: DateTime.now)
    render "index"
  end
end
