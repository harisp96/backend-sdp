class UserMailer < ApplicationMailer
  def welcome_email
   @user = params[:user]
   confirmation_token = SecureRandom.hex(10)
   @user.update!(confirmation_token: confirmation_token, confirmation_sent_at: DateTime.now)
   @url = "#{ENV["APP_ORIGIN"]}/confirm_emails?token=#{confirmation_token}"
   mail(to: @user.email, subject: 'Welcome to Event World')
 end

 def reservation_request
   @request = params[:request]
   @event = @request.event
   @user = @event.user
   mail(to: @user.email, subject: 'You have a new reservation request')
 end

 def confirm_reservation_request
   request = params[:request]
   @event = request.event
   @user = request.user
   mail(to: @user.email, subject: 'Your reservation request was approved')
 end

 def decline_reservation_request
   request = params[:request]
   @event = request.event
   @user = request.user
   @request = params[:request]
   mail(to: @user.email, subject: 'Your reservation request was declined')
 end
end
