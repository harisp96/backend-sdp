class ApplicationMailer < ActionMailer::Base
  default from: 'eventworldsdp@gmail.com'
  layout 'mailer'
end
