class EventParticipant::ParticipantWrapper
  attr_reader :opportunity

  def initialize(event)
    @event = event
  end

  def [](key)
    mapper[key]
  end

  def to_h
    mapper
  end

  private

    def mapper
      @_mapper ||= {
        Id:           @opportunity.crm_opportunity_id,
        Name:         @opportunity.name,
        Probability:  @opportunity.probability,
        CreatedDate:  @opportunity.crm_created_at.iso8601,
        StageName:    @opportunity.crm_stage,
        CloseDate:    @opportunity.crm_close_at.iso8601,
        IsClosed:     @opportunity.is_closed?,
        IsWon:        @opportunity.is_won?,
        "#{@amount_column}": @opportunity.amount,
        Owner: {
          CreatedDate: @opportunity.user.try(:created_at).try(:iso8601)
        }
      }.with_indifferent_access
    end
end
