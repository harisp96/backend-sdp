module ApplicationHelper
  def bio_resolver(bio)
    return bio if object.bio.length < 180
    return bio[0..180].gsub(/\s\w+\s*$/,'...') unless !object.bio.present?
    'Missing'
  end
end
