source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem 'simplecov', require: false, group: :test
gem 'rails', '~> 5.1.4'
gem 'pg', '~> 0.18'
gem 'bootstrap', '~> 4.3.1'
gem 'railroady'
gem 'rails-observers'
gem 'dropbox_api'
gem 'mini_magick'
gem 'google_places'
gem 'rack-cors', require: 'rack/cors'
gem 'responders'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'bcrypt'
gem 'reform', '~> 2.1.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'doorkeeper'
gem 'devise'
gem 'doorkeeper-jwt'
gem 'byebug'
gem "google-api-client"
gem 'geocoder'
gem "pundit"
gem 'rubocop'
gem 'rollbar'
gem 'active_model_serializers'
gem 'dotenv-rails', groups: [:development, :test]
gem 'rails-erd', group: :development

gem "bootstrap_form", ">= 4.0.0.alpha1"
# gem 'devise_invitable'
gem 'devise-bootstrap-form'



group :development, :test do
  gem 'rspec-rails'
  gem 'capybara', '~> 2.13'
  gem 'factory_bot_rails'
  gem 'selenium-webdriver'
end

group :test do
  gem 'dox', require: false
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
